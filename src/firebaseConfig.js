// import { initializeApp } from "firebase/app";
import firebase from 'firebase/compat/app'
import 'firebase/compat/auth';
import 'firebase/compat/database'

const firebaseConfig = {
apiKey: "AIzaSyAQnnrzRiYufTwlVKA2K_V63LmCmeBl4hc",
authDomain: "react-firebase-auth-4f523.firebaseapp.com",
projectId: "react-firebase-auth-4f523",
storageBucket: "react-firebase-auth-4f523.appspot.com",
messagingSenderId: "571474827389",
appId: "1:571474827389:web:4da9ccd394372b133eb914"
};

const app = firebase.initializeApp(firebaseConfig);
export default app