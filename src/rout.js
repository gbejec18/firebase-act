import React from "react";
import { Routes, Route } from "react-router-dom";
import Signup from "./signup";
import Login from "./login";
import HomePage from "./home";
const Rout = () => {
    return (
        <>
        <Routes>
            <Route path='/' element={<Signup />}></Route>
            <Route path='/login' element={<Login />} ></Route>
            <Route path='/home' element={<HomePage />} ></Route>

        </Routes>
        </>
    )
}

export default Rout