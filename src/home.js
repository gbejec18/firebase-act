import React from 'react';
import './home.css';

function HomePage() {
  return (
    <div className="homepage">
      <header className="homepage-header">
        <h1>Welcome to Our Website!</h1>
      </header>
      <main className="homepage-main">
        <p>This is a professional looking homepage. You can add more content here as needed.</p>
      </main>
      <footer className="homepage-footer">
        <p>© 2023 Our Website. All rights reserved.</p>
      </footer>
    </div>
  );
}

export default HomePage;
